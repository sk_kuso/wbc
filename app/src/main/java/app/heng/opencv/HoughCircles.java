package app.heng.opencv;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Heng on 23/11/2558.
 */
public class HoughCircles extends Activity {

    private ImageView img;
    private Mat src = new Mat();

    private Mat imgage = new Mat();
    private Mat src_gray;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ellipse);


        Bundle bundle = getIntent().getExtras();
        String nameLatestPhoto = bundle.getString("MyPhoto");
        Log.i("name of latest photo: ", nameLatestPhoto);

        //เชื่อม obj
        img = (ImageView) findViewById(R.id.imageView);

        Bitmap myBitmap = BitmapFactory.decodeFile(nameLatestPhoto);
        //ถอดรหัสไฟล์ภาพ จาก Uri
        //ถอดเสร็จให้เก็บในตัวแปรสำหรับเก็บภาพ Bitmap
        //img.setImageBitmap(myBitmap); // set ภาพให้ image view เป็นภาพแบบ bitmap


        // แปลงภาพเป็น Gray และ blur

        //Mat source = new Mat(myBitmap.getWidth(), myBitmap.getHeight(), CvType.CV_8UC1);
        Mat source = new Mat();

        Utils.bitmapToMat(myBitmap, source);

        // blur
//		Imgproc.medianBlur(source, source, 5);
		Imgproc.GaussianBlur(source, source, new Size(3, 3), 15);


        // convert to grayscale

        Imgproc.cvtColor(source, source, Imgproc.COLOR_RGB2GRAY);
        Imgproc.GaussianBlur(source, source, new Size(3, 3), 15);


        // do hough circles
        Mat circles = new Mat();
        int minRadius = 40;  //10
        int maxRadius = 98;  //18
        Imgproc.HoughCircles(source, circles, Imgproc.CV_HOUGH_GRADIENT, 1, minRadius, 120, 30, minRadius, maxRadius );
        //source, circles, Imgproc.CV_HOUGH_GRADIENT, 1, minRadius, 120, 10, minRadius, maxRadius //วาดวงรี ได้วงเล็กๆ
        //HoughCircles(gray, circles, CV_HOUGH_GRADIENT, 2, gray -> rows / 4, 200, 100);
        //Mat image, Mat circles,int method, double dp, double minDist, double param1, double param2, int minRadius, int maxRadius

        Log.w("circles", circles.cols() + "");

        for (int x = 0; x < circles.cols(); x++)
        {
            double vCircle[]=circles.get(0,x);

            Point pt = new Point(Math.round(vCircle[0]), Math.round(vCircle[1]));
            int radius = (int)Math.round(vCircle[2]);
            // draw the circle center
            Core.circle(source, pt, 13,new Scalar(0,255,0), -1, 8, 0 ); //13 -> 3
            // draw the circle outline
            Core.circle( source, pt, radius, new Scalar(0,0,255), 3, 8, 0 );

        }


        Utils.matToBitmap(source, myBitmap);

        img.setImageBitmap(myBitmap);
    }

}
