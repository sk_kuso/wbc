package app.heng.opencv;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;



public class MainActivityH extends AppCompatActivity  {

    private Button Btake;
    private Button Bselect;
    private Button Breal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        Btake = (Button)findViewById(R.id.button);
        Btake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i1 = new Intent(getApplicationContext(), Treal.class);
                startActivity(i1);
            }
        });

        Bselect = (Button)findViewById(R.id.button3);
        Bselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(getApplicationContext(), ColorBlobDetectionActivity.class);
                startActivity(i2);
            }
        });

        Breal = (Button)findViewById(R.id.button4);
        Breal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i3 = new Intent(getApplicationContext(), ColorBlobDetectionActivity.class);
                startActivity(i3);
            }
        });





    }




}
